"use strict";

for (var i = 1; i <= 10; i++) {
    $.ajax("https://swapi.dev/api/people/" + i + "/",{  // try $.get
    type: "GET",
    data: {
        limit: 10
    }
}).done(function (data) {
    console.log(data.results);
    var starWarsHTML = displayStarWars(data);
});

function displayStarWars(res) {

    var starWarsOnHTML = '';
    var name = res.name
    var height = res.height
    var mass = res.mass
    var birth_year = res.birth_year
    var gender = res.gender
    var homeworld = res.homeworld
    var films = res.films

        starWarsOnHTML += `
                    <tr>
                        <td scope="col">${name}</td>
                        <td scope="col">${height}</td>
                        <td scope="col">${mass}</td>
                        <td scope="col">${birth_year}</td>
                        <td scope="col">${gender}</td>
                        <td scope="col">${homeworld}</td>
                        <td scope="col">${films}</td>
                    </tr>`
        $("#insertCharacters").append(starWarsOnHTML)
    }
}